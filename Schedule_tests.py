#
#
#  TEST SCRIPT FOR SCHEDULE
#
#  This script does not do thorough testing of the schedule program, but rather displaying
#  its functionality, and showing how to use the most important features
#
#  By doing so, it should expose as much code as possible, and thus find the most common errors
#
#  JJD, 30/10/2019,  GPL v2 Licence
#
#
#
#




from Schedule import *

today = datetime.datetime.today()

def timeTest(device, year=today.year, month=today.month, day=today.day):
    '''Running through times and plotting states'''
    initial_time = device.conditions.now
    print('\n'*3)
    for i in range(24):
        device.conditions.now = datetime.datetime(year, month, day, i, 30, 00, 0)
        print("{} - {} : {}".format(device.name, device.conditions.now, device.state ) )
    device.conditions.now = initial_time

def announceSection(name):
    print('\n'*2 + '*'*150)
    print(name)
    print('*'*150 + '\n'*2 )



# opening schedule _with dry_mode_ on (will not talk to Domoticz)
devices = DEVICES( scheduleFile , scheduleTab, verbose=False, dry_run=True  )

# Normal behaviour
announceSection("Normal behaviour")
devices.toggle()
devices.report()

# Schedule
announceSection("Schedule captured for each device ({} devices found)".format(len(devices)))
for device in devices:
    device.print()

# Winning schedule
announceSection("Winning schedule for each device. Assumes Prog mode. i is the schedule index (0 is the first entered)")
for device in devices:
    print(device, '\t\t',  device.winningSchedule()  )


# conditions
announceSection("Conditions entered ({} user conditions found)".format(len(devices.conditions)))
print('\n'*3, devices.conditions)

announceSection("Some conditions testing. None output means that none of the conditions given are True")
print(today, 'is WD ?', devices("cuisine").conditions.evaluate(['WD'] ) )
print(today, 'is WE ?', devices("cuisine").conditions.evaluate(['WE'] ) )
print(today, 'is Monday, Tuesday or Wednesday ?', devices("cuisine").conditions.evaluate(['Monday', 'Tuesday', 'Wednesday'] ) )


# Time testing
announceSection("Time-testing first device in the list, i.e. sweeping through all hours of the day")
timeTest(devices(0))

# Info - now displayed in the report
#announceSection("Spreadsheet info captured")
#print( devices.info )


# Checking connection to Domoticz
#
#

# Notifications
announceSection("Testing Domoticz notification system...")
notification = NOTIFICATION(urlbase, username, passwd, None, verbose=False, dry_run=False )
notification.notify('test subject', 'test body')

print("Received notification ?")

# Switches
announceSection("Testing switch/Selector ")

# reopening devices with connection to Domoticz
devices = DEVICES( scheduleFile , scheduleTab, verbose=False, dry_run=False)

device = devices(0)
#if device.object == 'switch':
#    device.switches[0].set(True)
#if device.object == 'selector':
#    device.selectors[0].set(True)






